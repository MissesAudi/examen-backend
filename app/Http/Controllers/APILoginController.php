<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;;

class APILoginController extends Controller
{
    public function login(Request $request)
    {

//Validates de login data
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password'=> 'required'
        ]);

//If validator fails give error
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
//Credentials from login
        $credentials = $request->only('email', 'password');

        try {
//credentials invalid give 401
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
//could not create token credentials invalid
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
//Login because token
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {
//if token could not be authenticated give 404
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
// token expired give token expired
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
// token invald give token invalid
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
//token not found give token absent
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
// the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
}
