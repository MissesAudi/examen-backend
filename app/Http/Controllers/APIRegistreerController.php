<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;
//use Validator;
use Illuminate\Support\Facades\Validator;
use Response;


class APIRegisterController extends Controller
{
    public function register(Request $request)
    {

//        Check if everything is filled
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required'
        ]);

//        if the validator fails it gives an error
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

//      Creates new user
        User::create([
            'name' => $request->get('name'),
            'organisatie' => $request->get('organisatie'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        $user = User::first();
        $token = JWTAuth::fromUser($user);

        return Response::json(compact('token'));
    }
}
