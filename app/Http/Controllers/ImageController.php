<?php

namespace App\Http\Controllers;

use App\Image;
use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Input;


class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Image::all();
    }

//    Copy uploaded file and save in directory
    public function copyFile($file, $fileName, $extension, $folder = 'upload')
    {
        if (!is_dir(storage_path($folder))) {
            mkdir(storage_path($folder));
        }

        $target = $folder . '/' . $fileName .'.'. $extension;
        \File::copy($file,storage_path('app/public/' . $target));
        return '/' . $target;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'file'=>'required'
        ]);

        $file = new Image($request->only('path','size','mime_type'));
        $fileObject = Input::file('file');
        if($fileObject->isValid()){
            $fileSize = $fileObject->getClientSize();
            $extension = $fileObject->getClientOriginalExtension();
            $path = $this->copyFile($fileObject,uniqid(),$extension);
            $file->path = $path;
            $file->mime_type = $extension;
            $file->size = $fileSize;

            $file->save();
        }
        return $file;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Image::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $image = Image::find($id);
        $fileObject = Input::file('file');
        $fileSize = $fileObject->getClientSize();
        $extension = $fileObject->getClientOriginalExtension();
        $path = $this->copyFile($fileObject,uniqid(),$extension);
        $image->path = $path;
        $image->mime_type = $extension;
        $image->size = $fileSize;
        $image->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
