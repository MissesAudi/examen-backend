<?php

namespace App\Http\Controllers;

use App\Grocery;
use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Receipt::with('image','user')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Receipt::create([
            'name' => $request->name,
            'description' => $request->description,
            'image_id' => $request->image_id,
            'user_id' => Auth::user()->id,
            'checked' => false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Receipt::with('image','user')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $receipt = Receipt::find($id);
        $receipt->name = Input::get('name');
        $receipt->description = Input::get('description');
        $receipt->checked = Input::get('checked');
        $receipt->image_id = Input::get('image_id');

        $receipt->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $receipt = Receipt::find($id);
        $receipt ->delete();

        return Response::json(array(
            'succes'=> true,
            'data' => $receipt
        ));
    }
}
