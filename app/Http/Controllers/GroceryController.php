<?php

namespace App\Http\Controllers;

use App\Grocery;
use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Response;

class GroceryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Grocery::with('image')->get();
    }

    public function checked($id)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
//            'description'=>'required',
        ]);

        return Grocery::create([
            'name' => $request->name,
            'description' => $request->description,
            'image_id' => $request->image_id,
            'checked'=> false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grocery  $grocery
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Grocery::with('image')->find($id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grocery  $grocery
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $grocery = Grocery::find($id);
        $grocery->name = Input::get('name');
        $grocery->description = Input::get('description');
        $grocery->checked = Input::get('checked');
        $grocery->image_id = Input::get('image_id');

        $grocery->save();

    }



        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grocery  $grocery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grocery = Grocery::find($id);
        $grocery ->delete();

        return Response::json(array(
            'succes'=> true,
            'data' => $grocery
        ));
    }
}
