<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
//    Data can now be uploaded too these rows
    protected $fillable = [
        'name', 'description', 'file','image_id','checked','user_id'
    ];

//    Data from App\Image can be picked up with these ids because there linked now

    public function image(){
        return $this->belongsTo('App\Image');
    }

//    Data from App\User can be picked up with these ids because there linked now
    public function user(){
        return $this->belongsTo('App\User');
    }
}
