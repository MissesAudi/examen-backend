<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grocery extends Model
{

//    These rows can be filled now
    protected $fillable = [
            'name', 'description', 'file', 'image_id','checked'
    ];

//    Now Grocery is linked to App\Image so they can pick up the data

    public function image(){
        return $this->belongsTo('App\Image');
    }
}
